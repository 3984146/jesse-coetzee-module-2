import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/appwinner.dart';
import 'package:flutter_application_1/dashboard.dart';
import 'package:flutter_application_1/feature2.dart';
import 'package:flutter_application_1/profile.dart';
import 'package:flutter_application_1/signup.dart';
import 'loginpage.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MTNAcadamyApp());
}

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      splash: Column(
        children: [
          Image.asset('assets/logo.png'),
          const Text(
            'MTN Acadamy',
            style: TextStyle(
                fontSize: 40, fontWeight: FontWeight.bold, color: Colors.white),
          ),
        ],
      ),
      backgroundColor: Colors.yellow,
      nextScreen: const LoginPage(),
      splashIconSize: 250,
    );
  }
}

class MTNAcadamyApp extends StatelessWidget {
  const MTNAcadamyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MTN Acadamy App',
      theme: ThemeData(
        splashColor: Colors.yellow,
        primarySwatch: Colors.amber,
        scaffoldBackgroundColor: Colors.blueGrey,
      ),
      home: const SplashScreen(),
      routes: {
        '/signup': (_) => const SignUpScreen(),
        '/login': (_) => const LoginPage(),
        '/dash': (context) => const DashBoard(),
        '/appwinner': (context) => const AppWinner(),
        '/feature2': (context) => const Feature2(),
        '/profile': (context) => const Profile(),
      },
    );
  }
}
