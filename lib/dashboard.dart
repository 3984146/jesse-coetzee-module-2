// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, avoid_unnecessary_containers

import 'package:flutter/material.dart';

class DashBoard extends StatelessWidget {
  const DashBoard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('MTN Acadamy'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(top: 8),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          minimumSize: Size(180, 50),
                          textStyle: TextStyle(fontSize: 28)),
                      onPressed: () {
                        Navigator.of(context).pushNamed('/appwinner');
                      },
                      child: Text('App Winner'))),
              Container(
                  margin: EdgeInsets.only(top: 8),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          minimumSize: Size(180, 50),
                          textStyle: TextStyle(fontSize: 28)),
                      onPressed: () {
                        Navigator.of(context).pushNamed('/feature2');
                      },
                      child: Text('Feature 2'))),
              Container(
                  margin: EdgeInsets.only(top: 8),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          minimumSize: Size(180, 50),
                          textStyle: TextStyle(fontSize: 28)),
                      onPressed: () {
                        Navigator.of(context).pushNamed('/profile');
                      },
                      child: Text('Edit Profile'))),
            ],
          ),
        ));
  }
}
